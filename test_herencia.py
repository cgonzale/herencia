from practica_herencia import Persona
from practica_herencia import Paciente
from practica_herencia import Medico
import unittest

class TestHerencia(unittest.TestCase):

    def test_historial_clinico(self):
        paciente=Paciente ("Carla", "Gonzalez", "10/02/2005", "654368858G", "Enferma del corazon" )
        historial=paciente.ver_historial_clinico()
        self.assertEqual(historial, "Historial clinico: Enferma del corazon")

    def test_consultar_agenda (self):
        medico=Medico ("Roberto", "Franco", "26/06/1986", "865467865G", "Cirugia", "12/12/2012 a las 12:12")
        agenda=medico.consultar_agenda()
        self.assertEqual (agenda, "Hay una cita programada el 12/12/2012 a las 12:12 en la especialidad de Cirugia")

if __name__ == "__main__":
    unittest.main()
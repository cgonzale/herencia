class Persona:
    def __init__ (self, nombre, apellidos, nacimiento, DNI):
        self.nombre = nombre
        self.apellidos = apellidos
        self.nacimiento = nacimiento
        self.DNI = DNI

    def setNombre (self, nombre):
        self.nombre=nombre
    
    def getNombre (self):
        return self.nombre

    def setApellidos (self, apellidos):
        self.apellidos=apellidos

    def getApellidos(self):
        return self.apellidos

    def setNacimiento(self, nacimiento):
        self.nacimiento=nacimiento

    def getNacimiento(self):
        return self.nacimiento

    def setDNI(self, DNI):
        self.DNI=DNI

    def getDNI(self):
        return self.DNI

    def __str__(self):
        return "La persona se llama {name} {surname}, nacio el {date} y tiene DNI: {DNi}".format(name=self.nombre, surname=self.apellidos, date=self.nacimiento, DNi=self.DNI)

class Paciente (Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, hist):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self.historial_clinico = hist

    def ver_historial_clinico (self):
        return "Historial clinico: {historial}".format(historial=self.historial_clinico)

class Medico (Persona):
    def __init__(self, nombre, apellidos, nacimiento, DNI, espec, cit):
        super().__init__(nombre, apellidos, nacimiento, DNI)
        self.especialidad=espec
        self.cita=cit

    def consultar_agenda(self):
        return "Hay una cita programada el {cit} en la especialidad de {espec}".format(cit=self.cita, espec=self.especialidad)


persona=Persona ("Carla", "González", "10/02/2005", "567943278F")
paciente=Paciente("Jimena", "Maestro", "05/12/2006", "328646809J", "Enferma de gripe")
medico=Medico ("Roberto", "Franco", "31/04/2006", "532576897K", "Cirugia", "12/12/2012 a las 12:12")


gente=[persona, paciente, medico]
for people in gente:
    print (people)
    if people==paciente:
        print (people.ver_historial_clinico())
    elif people==medico:
        print(people.consultar_agenda())

